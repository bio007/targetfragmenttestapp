package com.sygic.fragmentresults;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static com.sygic.fragmentresults.MainActivity.TAG;


public class MainFragment extends Fragment implements View.OnLongClickListener {

    private String textData = null;

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate Main - " + this);

        MainActivity.sListeners.add(this);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView Main");

        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        final TextView textView = view.findViewById(R.id.label);
        if (textData == null && savedInstanceState != null) {
            textData = savedInstanceState.getString("textData");
        }

        if (textData != null) {
            textView.setText(textData);
        }

        view.findViewById(R.id.load_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final WorkerFragment fragment = new WorkerFragment();
                fragment.setTargetFragment(MainFragment.this, 331);
                getFragmentManager().beginTransaction().replace(R.id.content, fragment).addToBackStack("worker").commit();
            }
        });
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("textData", textData);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case 331:
                textData = data.getStringExtra("input");
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Log.d(TAG, "onDestroyView Main");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "onDestroy Main");

        MainActivity.sListeners.remove(this);
    }

    @Override
    public boolean onLongClick(final View v) {
        return false;
    }
}
